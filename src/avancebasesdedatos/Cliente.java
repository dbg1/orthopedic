package avancebasesdedatos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jeffrey Prado
 */
public class Cliente {

    Connection cn;

    public void CargarTabla(JTable tabla, String cadena) {
        DefaultTableModel modelo;
        String[] titulo = {"CEDULA", "NOMBRE", "APELLIDO", "CORREO", "TELEFONO"};
        modelo = new DefaultTableModel(null, titulo);

        String[] registros = new String[5];
        String sql = "SELECT cedula, nombre, apellido, correo, telefono FROM cliente WHERE CONCAT_WS(' ',cedula, nombre, apellido, correo, telefono) LIKE '%" + cadena + "%'";
        Conexion con = new Conexion();
        cn = con.conectar();

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                for (int i = 0; i < 5; i++) {
                    registros[i] = rs.getString(i + 1);
                }
                modelo.addRow(registros);
            }
            tabla.setModel(modelo);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error: " + ex);
        }
    }

    public void RegistrarCliente(JTextField cedula, JTextField nombre, JTextField apellido, JTextField correo, JTextField telefono) {
        try {
            Conexion con = new Conexion();
            cn = con.conectar();

            String sql = "CALL REGISTRAR_CLIENTE (?,?,?,?,?)";

            PreparedStatement pst = cn.prepareCall(sql);

            pst.setString(1, cedula.getText());
            pst.setString(2, nombre.getText());
            pst.setString(3, apellido.getText());
            pst.setString(4, correo.getText());
            pst.setString(5, telefono.getText());

            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "SE HA REGISTRADO UN NUEVO CLIENTE");

            pst.close();
            cn.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void ModificarCliente(JTextField cedula, JTextField nombre, JTextField apellido, JTextField correo, JTextField telefono) {
        try {
            Conexion con = new Conexion();
            cn = con.conectar();

            String sql = "CALL MODIFICAR_CLIENTE (?,?,?,?,?)";

            PreparedStatement pst = cn.prepareCall(sql);

            pst.setString(1, cedula.getText());
            pst.setString(2, nombre.getText());
            pst.setString(3, apellido.getText());
            pst.setString(4, correo.getText());
            pst.setString(5, telefono.getText());

            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "SE HA MODIFICADO CORRECTAMENTE");

            pst.close();
            cn.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    void EliminarCliente(JTextField cedula) {
        try {
            Conexion con = new Conexion();
            cn = con.conectar();

            String sql = "CALL ELIMINAR_CLIENTE (?)";

            PreparedStatement pst = cn.prepareCall(sql);

            pst.setString(1, cedula.getText());

            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "SE HA ELIMINADO CORRECTAMENTE");

            pst.close();
            cn.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

}
