/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avancebasesdedatos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Alexis Poveda
 */
public class Vende {

    Connection cn;

    public void CargarTabla(JTable tabla, String cadena) {
        DefaultTableModel modelo;
        String[] titulo = {"CODIGO", "NOMBRE PRODUCTO", "PRECIO VENTA", "STOCK"};
        modelo = new DefaultTableModel(null, titulo);

        String[] registros = new String[4];
        String sql = "SELECT codProducto, nomProducto, precioProducto, stockProducto FROM producto WHERE CONCAT_WS(' ',codProducto,nomProducto,precioProducto,stockProducto) LIKE '%" + cadena + "%'";
        Conexion con = new Conexion();
        cn = con.conectar();

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                for (int i = 0; i < 4; i++) {
                    registros[i] = rs.getString(i + 1);
                }
                modelo.addRow(registros);
            }
            tabla.setModel(modelo);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error: " + ex);
        }
    }



    public void Agregar(String id,String factura ,String codigo,String cantidad,String precioproducto) {
        try {
            Conexion con = new Conexion();
            cn = con.conectar();
            
            String sql = "CALL AGREGAR_VENTA (?,?,?,?,?)";
            
            PreparedStatement pst = cn.prepareCall(sql);

            pst.setString(1, id);
            pst.setString(2, factura);
            pst.setString(3, codigo);
            pst.setString(4, cantidad);
            pst.setString(5, precioproducto);
       
            pst.executeUpdate();
            //JOptionPane.showMessageDialog(null, "AGREGADO AL CARRITO");
            pst.close();
            cn.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    public void Facturar(String numFactura,String total) {
        try {
            Conexion con = new Conexion();
            cn = con.conectar();
            
            String sql = "CALL FACTURAR (?,?)";
            
            PreparedStatement pst = cn.prepareCall(sql);

            pst.setString(1, numFactura);
            pst.setString(2, total);
       
            pst.executeUpdate();
            //JOptionPane.showMessageDialog(null, "AGREGADO AL CARRITO");
            pst.close();
            cn.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
