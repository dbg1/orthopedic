/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avancebasesdedatos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Alexis Poveda
 */
public class Proveedor {
    Connection cn;

    public void CargarTabla(JTable tabla, String cadena) {
        DefaultTableModel modelo;
        String[] titulo = {"RUC", "NOMBRE", "DIRECCION", "TELEFONO"};
        modelo = new DefaultTableModel(null, titulo);

        String[] registros = new String[4];
        String sql = "SELECT idProveedor, nomProveedor, dirProveedor, telefProveedor FROM proveedor  WHERE CONCAT_WS(' ',idProveedor, nomProveedor, dirProveedor, telefProveedor) LIKE '%" + cadena + "%'";
        Conexion con = new Conexion();
        cn = con.conectar();

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                for (int i = 0; i < 4; i++) {
                    registros[i] = rs.getString(i + 1);
                }
                modelo.addRow(registros);
            }
            tabla.setModel(modelo);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error: " + ex);
        }
    }

    public void RegistrarProveedor(JTextField ruc, JTextField nombre, JTextField direccion, JTextField telefono) {
        try {
            Conexion con = new Conexion();
            cn = con.conectar();

            String sql = "CALL REGISTRAR_PROVEEDOR (?,?,?,?)";

            PreparedStatement pst = cn.prepareCall(sql);

            pst.setString(1, ruc.getText());
            pst.setString(2, nombre.getText());
            pst.setString(3, direccion.getText());
            pst.setString(4, telefono.getText());
            

            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "SE HA REGISTRADO UN NUEVO PROVEEDOR");

            pst.close();
            cn.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void ModificarProveedor(JTextField ruc, JTextField nombre, JTextField direccion, JTextField telefono) {
        try {
            Conexion con = new Conexion();
            cn = con.conectar();

            String sql = "CALL MODIFICAR_PROVEEDOR (?,?,?,?)";

            PreparedStatement pst = cn.prepareCall(sql);

            pst.setString(1, ruc.getText());
            pst.setString(2, nombre.getText());
            pst.setString(3, direccion.getText());
            pst.setString(4, telefono.getText());
            

            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "SE HA MODIFICADO CORRECTAMENTE");

            pst.close();
            cn.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    void EliminarProveedor(JTextField ruc) {
        try {
            Conexion con = new Conexion();
            cn = con.conectar();

            String sql = "CALL ELIMINAR_PROVEEDOR (?)";

            PreparedStatement pst = cn.prepareCall(sql);

            pst.setString(1, ruc.getText());

            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "SE HA ELIMINADO CORRECTAMENTE");

            pst.close();
            cn.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
