/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avancebasesdedatos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Alexis Poveda
 */
public class venta {
     Connection cn;

    public void CargarTabla(JTable tabla, String cadena) {
        DefaultTableModel modelo;
        String[] titulo = {"NUMERO VENTA","CODIGO", "NOMBRE PRODUCTO","TOTAL","FECHA Y HORA"};
        modelo = new DefaultTableModel(null, titulo);

        String[] registros = new String[5];
        String sql1 = "SELECT * FROM ventascompletas WHERE CONCAT_WS(' ', numeroVenta, codigoProducto, nombreProducto, totalVenta, fechaHoraVenta) LIKE '%" + cadena + "%'";
        
        Conexion con = new Conexion();
        cn = con.conectar();

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql1);

            while (rs.next()) {
                for (int i = 0; i < 5; i++) {
                    registros[i] = rs.getString(i + 1);
                }
                modelo.addRow(registros);
            }
            tabla.setModel(modelo);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error: " + ex);
        }
    }
    public void CargarTablaDias(JTable tabla,String dia, String mes, String anio) {
        DefaultTableModel modelo;
        String[] titulo = {"NUMERO VENTA","CODIGO", "NOMBRE PRODUCTO","CANTIDAD" ,"TOTAL","FECHA Y HORA"};
        modelo = new DefaultTableModel(null, titulo);
        //'2018-10-21 11:00:12'
        String fecha = anio+"-"+mes+"-"+dia;
        String[] registros = new String[5];
        String sql1 = "SELECT * FROM ventasCompletas WHERE DAY(fechaHoraVenta) = "+dia+" AND MONTH(fechaHoraVenta)= "+mes+" AND YEAR(fechaHoraVenta)="+anio+";";
        //String sql = "SELECT numeroVenta, codigoProducto, nombreProducto, cantProducto, totalVenta, fechaHoraVenta FROM venta WHERE CONCAT_WS(' ',numeroVenta, codigoProducto, nombreProducto, cantProducto, totalVenta, fechaHoraVenta) LIKE '%" + cadena + "%'";
        Conexion con = new Conexion();
        cn = con.conectar();
           
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql1);

            while (rs.next()) {
                for (int i = 0; i < 5; i++) {
                    registros[i] = rs.getString(i + 1);
                }
                modelo.addRow(registros);
            }
            tabla.setModel(modelo);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error: " + ex);
        }
    }
    public void CargarTablaMes(JTable tabla, String mes, String anio) {
        DefaultTableModel modelo;
        String[] titulo = {"NUMERO VENTA","CODIGO", "NOMBRE PRODUCTO","CANTIDAD" ,"TOTAL","FECHA Y HORA"};
        modelo = new DefaultTableModel(null, titulo);
        //'2018-10-21 11:00:12'
        String[] registros = new String[5];
        String sql1 = "SELECT * FROM ventasCompletas WHERE MONTH(fechaHoraVenta)= "+mes+" AND YEAR(fechaHoraVenta)="+anio+";";
        //String sql = "SELECT numeroVenta, codigoProducto, nombreProducto, cantProducto, totalVenta, fechaHoraVenta FROM venta WHERE CONCAT_WS(' ',numeroVenta, codigoProducto, nombreProducto, cantProducto, totalVenta, fechaHoraVenta) LIKE '%" + cadena + "%'";
        Conexion con = new Conexion();
        cn = con.conectar();
           
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql1);

            while (rs.next()) {
                for (int i = 0; i < 5; i++) {
                    registros[i] = rs.getString(i + 1);
                }
                modelo.addRow(registros);
            }
            tabla.setModel(modelo);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error: " + ex);
        }
    }
    
    public void CargarTablaAnio(JTable tabla,String anio) {
        DefaultTableModel modelo;
        String[] titulo = {"NUMERO VENTA","CODIGO", "NOMBRE PRODUCTO","CANTIDAD" ,"TOTAL","FECHA Y HORA"};
        modelo = new DefaultTableModel(null, titulo);
        //'2018-10-21 11:00:12'
        String[] registros = new String[6];
        String sql1 = "SELECT * FROM ventasCompletas WHERE YEAR(fechaHoraVenta)="+anio+";";
        //String sql = "SELECT numeroVenta, codigoProducto, nombreProducto, cantProducto, totalVenta, fechaHoraVenta FROM venta WHERE CONCAT_WS(' ',numeroVenta, codigoProducto, nombreProducto, cantProducto, totalVenta, fechaHoraVenta) LIKE '%" + cadena + "%'";
        Conexion con = new Conexion();
        cn = con.conectar();
           
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql1);

            while (rs.next()) {
                for (int i = 0; i < 6; i++) {
                    registros[i] = rs.getString(i + 1);
                }
                modelo.addRow(registros);
            }
            tabla.setModel(modelo);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error: " + ex);
        }
    }
}
