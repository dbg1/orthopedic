/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avancebasesdedatos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Alexis Poveda
 */
public class Factura {
     Connection cn;

    public void CargarTabla(JTable tabla, String cadena) {
        DefaultTableModel modelo;
        String[] titulo = {"Nº FACTURA","CEDULA","NOMBRE", "CORREO","FECHA" ,"CANTIDAD DE PRODUCTOS", "TOTAL FACTURADO"};
        modelo = new DefaultTableModel(null, titulo);

        String[] registros = new String[7];
        String sql1 = "SELECT * FROM facturasCompletas WHERE CONCAT_WS(' ', numeroFactura, cedula, nombre, correo, fechaCompra, cantidadTotalProductos ,totalFacturado) LIKE '%" + cadena + "%'";
        //String sql = "SELECT numeroVenta, codigoProducto, nombreProducto, cantProducto, totalVenta, fechaHoraVenta FROM venta WHERE CONCAT_WS(' ',numeroVenta, codigoProducto, nombreProducto, cantProducto, totalVenta, fechaHoraVenta) LIKE '%" + cadena + "%'";
        Conexion con = new Conexion();
        cn = con.conectar();

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql1);

            while (rs.next()) {
                for (int i = 0; i < 7; i++) {
                    registros[i] = rs.getString(i + 1);
                }
                modelo.addRow(registros);
            }
            tabla.setModel(modelo);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error: " + ex);
        }
    }
    public void CargarTabla2(JTable tabla, String cadena, String cedu) {
        DefaultTableModel modelo;
        String[] titulo = {"Nº FACTURA","CEDULA","NOMBRE", "CORREO","FECHA" ,"TOTAL"};
        modelo = new DefaultTableModel(null, titulo);

        String[] registros = new String[6];
        String sql1 = "SELECT * FROM facturasCompletas WHERE cedula = "+cedu+";";
        //String sql = "SELECT numeroVenta, codigoProducto, nombreProducto, cantProducto, totalVenta, fechaHoraVenta FROM venta WHERE CONCAT_WS(' ',numeroVenta, codigoProducto, nombreProducto, cantProducto, totalVenta, fechaHoraVenta) LIKE '%" + cadena + "%'";
        Conexion con = new Conexion();
        cn = con.conectar();

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql1);

            while (rs.next()) {
                for (int i = 0; i < 6; i++) {
                    registros[i] = rs.getString(i + 1);
                }
                modelo.addRow(registros);
            }
            tabla.setModel(modelo);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error: " + ex);
        }
    }
}
