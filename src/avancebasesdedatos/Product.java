/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avancebasesdedatos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Alexis Poveda
 */
public class Product {
   Connection cn;
   public void CargarTabla(JTable tabla, String cadena){
        DefaultTableModel modelo;
        String [] titulo = {"CODIGO","PROVEEDOR","NOMBRE PROVEEDOR","CATEGORIA","NOMBRE PRODUCTO","PRECIO VENTA","PRECIO COSTO","STOCK"};
	modelo = new DefaultTableModel(null, titulo);

        String [] registros = new String[8];
        String sql = "SELECT * FROM productosCompletos WHERE CONCAT_WS(' ',codProducto, idProveedor, nomProveedor, tipoProducto, nomProducto, precioProducto, precioCosto, stockProducto) LIKE '%" + cadena + "%'";
        Conexion con=new Conexion();
        cn=con.conectar();
        
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            
            while(rs.next()){
                for(int i=0;i<8;i++)
                    registros[i]=rs.getString(i+1);
                modelo.addRow(registros);
            }
            tabla.setModel(modelo); 
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Error: "+ ex);
        }
    }
   public void RegistrarCliente(JTextField id, JComboBox proveedor,JTextField tipo,JTextField nombre,JTextField precioProducto,JTextField precioCosto,JTextField stock){
        try{
            Conexion con = new Conexion();
            cn=con.conectar();
            
            String sql="CALL REGISTRAR_PRODUCTO (?,?,?,?,?,?,?)";                    
            
            PreparedStatement pst = cn.prepareCall(sql);                             
                                                   
            pst.setString(1, id.getText());                                              
            pst.setString(2, String.valueOf(proveedor.getSelectedItem()));
            pst.setString(3, tipo.getText());
            pst.setString(4, nombre.getText());
            pst.setString(5, precioProducto.getText());
            pst.setString(6, precioCosto.getText());
            pst.setString(7, stock.getText());
            
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "SE HA REGISTRADO UN NUEVO PRODUCTO");
                                                                    
            pst.close();                                                            
            cn.close();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }
   public void ModificarProducto(JTextField id, JComboBox proveedor,JTextField tipo,JTextField nombre,JTextField precioProducto,JTextField precioCosto,JTextField stock){
        try{
            Conexion con = new Conexion();
            cn=con.conectar();
            
            String sql="CALL MODIFICAR_PRODUCTO (?,?,?,?,?,?,?)";                    
            
            PreparedStatement pst = cn.prepareCall(sql);                             
                                                   
            pst.setString(1, id.getText());                                              
            pst.setString(2, String.valueOf(proveedor.getSelectedItem()));
            pst.setString(3, tipo.getText());
            pst.setString(4, nombre.getText());
            pst.setString(5, precioProducto.getText());
            pst.setString(6, precioCosto.getText());
            pst.setString(7, stock.getText());
            
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "SE HA MODIFICADO CORRECTAMENTE");
                                                                    
            pst.close();                                                            
            cn.close();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    void EliminarProducto(JTextField id) {
        try{
            Conexion con = new Conexion();
            cn=con.conectar();
            
            String sql="CALL ELIMINAR_PRODUCTO (?)";                    
            
            PreparedStatement pst = cn.prepareCall(sql);                             
                                                   
            pst.setString(1, id.getText());                                              
          
            
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "SE HA ELIMINADO CORRECTAMENTE");
                                                                    
            pst.close();                                                            
            cn.close();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    
   
    
}
